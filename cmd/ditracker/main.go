package main

import(
	"fmt"
	"os"
	"gitlab.com/fidencio.garrido/ditracker/pkg/tracker"
)

func main() {
	client, err := tracker.NewClient()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	resp, err := client.GetImageList("node")
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	for _, image := range resp {
		fmt.Printf(`
Official: %t
Name:     %s
Stars:    %d
Desc:     %s
		`, image.IsOfficial, image.Name, image.StarCount, image.Description)
	}
}
