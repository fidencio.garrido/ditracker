package main

import(
	"net/http"
	"io"
	"encoding/json"
	"fmt"
	"strings"
	"strconv"
)

type Tag struct {
	Layer string `json:"layer"`
	Name string `json:"name"`
}

func isTracked(name string) bool {
	if strings.Contains(name, "alpine") && strings.HasPrefix(name, "16"){
		return true
	}
	return false
}

type Version struct {
	Major int64
	Minor int64
	Patch int64
}

func getVersion(from string) (Version, error) {
	imageParts := strings.Split(from, "-")
	versionParts := strings.Split(imageParts[0], ".")
	if len(versionParts) < 3 {
		return Version{}, fmt.Errorf("invalid version %s", from)
	}
	major, _ := strconv.ParseInt(versionParts[0], 10, 64)
	minor, _ := strconv.ParseInt(versionParts[1], 10, 64)
	patch, _ := strconv.ParseInt(versionParts[2], 10, 64)
	return Version{major, minor, patch}, nil
}


func main() {
	image := "node"
	fullURL := fmt.Sprintf("https://registry.hub.docker.com/v1/repositories/%s/tags", image)
	fmt.Printf("Calling %s\n", fullURL)
	client := http.DefaultClient
	req, err := http.NewRequest(http.MethodGet, fullURL, nil)
	if err != nil {
		panic(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	var tags []Tag
	err = json.Unmarshal(data, &tags)
	if err != nil{
		panic(err)
	}
	wantedVersions := make([]string, 0)
	for _, t := range tags {
		if isTracked(t.Name){
			wantedVersions = append(wantedVersions, t.Name)
			fmt.Println(t.Name)
			version, err := getVersion(t.Name)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println(version)
			}
		}
	}
	
}
