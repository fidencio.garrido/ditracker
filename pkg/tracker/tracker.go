package tracker

import "context"
import "github.com/docker/docker/api/types"
import "github.com/docker/docker/api/types/registry"
import "github.com/docker/docker/client"

// DockerClient connection to the docker registry
type DockerClient struct {
	client *client.Client
}

// ImageMetadata docker image information
type ImageMetadata struct {
	Version string `json:"version"`
}

func NewClient() (*DockerClient, error) {
	//ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, 
		client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}
	return &DockerClient{
		client: cli,
	}, nil
}

// GetImageList retrieves list of available versions of a given image
func (c *DockerClient) GetImageList(imageName string) ([]registry.SearchResult, error) {
	ctx := context.Background()
	opts := types.ImageSearchOptions {
		Limit: 10,
	}
	res, err := c.client.ImageSearch(ctx, imageName, opts)
	if err != nil {
		return nil, err
	}
	return res, nil
}

